# 1. 
# 2. Mampu mendemonstrasikan pembuatan Docker Image melalui Dockerfile, sebagai bentuk implementasi dari sistem operasi.

Untuk membuat Docker Image menggunakan Dockerfile bisa sebagai berikut:
1. Base Image: Pertama, Dockerfile dimulai dengan pernyataan FROM debian:bullseye-slim. Ini menentukan bahwa Docker Image akan didasarkan pada imajinasi Debian dengan versi `bullseye-slim`. Base Image menyediakan sistem operasi dasar untuk menjalankan kontainer.

2. Install Dependensi: Selanjutnya, pernyataan RUN apt-get update && apt-get install -y git neovim netcat python3-pip digunakan untuk mengupdate paket dan menginstal dependensi yang diperlukan dalam kontainer. Dependensi yang diinstal termasuk git, neovim, netcat, dan python3-pip.

3. Copy File Aplikasi: Pernyataan COPY hadits-d.py /home/hadits-digital/hadits-d.py dan COPY requirements.txt /home/hadits-digital/requirements.txt digunakan untuk menyalin file aplikasi `thrifting_pakaian.py` dan file `requirements.txt` ke dalam kontainer. File aplikasi dan file dependensi ini akan digunakan dalam langkah-langkah selanjutnya.

4. Install Dependensi Python: Pernyataan RUN pip3 install --no-cache-dir -r /home/thrifting-pakaian/requirements.txt digunakan untuk menginstal dependensi Python yang diperlukan dalam aplikasi. File requirements.txt berisi daftar dependensi Python yang dibutuhkan oleh aplikasi.

5. Set Working Directory: Pernyataan WORKDIR /home/hadits-digital menetapkan direktori kerja di dalam kontainer sebagai `/home/thrifting-pakaian`. Ini adalah direktori di mana file-file aplikasi dan dependensi akan berada.

6. Expose Port: Pernyataan EXPOSE 25029 digunakan untuk mengekspos port 25029 di dalam kontainer. Port ini akan digunakan untuk mengakses aplikasi `Thrifting Pakaian` yang berjalan dalam kontainer.

7. Start Web Service: Pernyataan CMD ["streamlit", "run", "--server.port=25029", "thrifting_pakaian.py"] digunakan untuk menjalankan perintah saat kontainer dimulai. Pada kasus ini, perintah streamlit run --server.port=25029 thrifting_pakaian.py akan dijalankan untuk menjalankan aplikasi Streamlit.
Setelah Dockerfile selesai, Anda dapat menggunakan perintah docker build untuk membangun Docker Image dari Dockerfile tersebut. Contoh perintahnya adalah:

# 3. Mampu mendemonstrasikan pembuatan web page / web service sederhana berdasarkan permasalahan dunia nyata yang dijalankan di dalam Docker Container.
Langkah-langkah:

1. Persiapan Proyek:
- Buat direktori baru untuk proyek dan masuk ke dalamnya.
- Buat file Dockerfile untuk mendefinisikan Docker image yang akan digunakan.
- Buat file requirements.txt untuk mengelola dependensi Python yang diperlukan.
- Buat file thrifting_pakaian.py yang akan berisi kode aplikasi web.
2. Konfigurasi Docker:
- Buka file Dockerfile dan tulis kode berikut:
```
# Menggunakan base image Python
FROM python:3.9-slim

# Menyalin file dependensi ke dalam image
COPY requirements.txt .

# Menginstal dependensi yang diperlukan
RUN pip install --no-cache-dir -r requirements.txt

# Menyalin kode aplikasi ke dalam image
COPY thrifting_pakaian.py .

# Menjalankan perintah saat container dimulai
CMD ["python", "thrifting_pakaian.py"]
```
3. Kode Aplikasi
- Buka file thrifting_pakaian.py dan tulis kode berikut:
```
FROM debian:bullseye-slim

#install
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

#Copy web service files
COPY thrifting_pakaian.py /home/thrifting-pakaian/thrifting_pakaian.py
COPY requirements.txt /home/thrifting-pakaian/requirements.txt

#install python
RUN pip3 install --no-cache-dir -r /home/thrifting-pakaian/requirements.txt

#set working directory
WORKDIR /home/thrifting-pakaian

#expose port
EXPOSE 25029

#start web service
CMD ["streamlit", "run", "--server.port=25029", "thrifting_pakaian.py"]
```

# 4. Mampu mendemonstrasikan penggunaan Docker Compose pada port tertentu sebagai bentuk kontainerisasi program dan sistem operasi yang menjadi dasar distributed computing.
1. Persiapan Proyek:
- Buat direktori baru untuk proyek dan masuk ke dalamnya.
- Buat file docker-compose.yml untuk mendefinisikan layanan yang akan dijalankan.
2. Konfigurasi Docker Compose:
Buka file docker-compose.yml dan tulis kode berikut:
```
version: "3"
services:
  thrifting-pakaian:
    image: sidapaaaa/thrifting-pakaian:1.0
    ports:
              - "25029:25029"
    networks :
              - niat-yang-suci
    volumes :
              - /home/praktikumc/1217050029/thrifting-pakaian:/thrifting-pakaian

networks:
    niat-yang-suci:
      external : true
```

# 5. Mampu menjelaskan project yang dibuat dalam bentuk file README.md

- Deskripsi project
Deskripsi project
Program tersebut adalah sebuah aplikasi sederhana yang menggunakan framework Streamlit dan library Pandas di Python untuk membuat tampilan interaktif yang memungkinkan pengguna untuk mencari dan menampilkan barang yang akan di beli. Berikut ini adalah penjelasan lebih detail mengenai setiap bagian dari program tersebut:

1. Import Library
```
import streamlit as st
import requests
```
Kode di atas mengimpor dua library yang digunakan dalam program ini, yaitu `streamlit` dan `requests`. streamlit adalah library yang digunakan untuk membuat antarmuka web, sedangkan requests adalah library yang digunakan untuk melakukan permintaan HTTP ke API.

2. Daftar Barang Thrifting
```
daftarBarang = [
    {"Nama Barang": "Kemeja Flanel", "Harga": 30000},
    {"Nama Barang": "Celana Jeans", "Harga": 50000},
    {"Nama Barang": "Jaket Kulit", "Harga": 75000},
    {"Nama Barang": "Sepatu Sneakers", "Harga": 60000},
    {"Nama Barang": "Tas Vintage", "Harga": 40000}
]
```
Variabel `daftarBarang` adalah daftar barang thrift yang tersedia. Setiap barang direpresentasikan sebagai sebuah dictionary dengan kunci "Nama Barang" dan "Harga".

3. Fungsi `tampilkanDaftarBarang()`:
```
def tampilkanDaftarBarang(daftarBarang):
    st.write("Daftar Barang Thrift:")
    st.write("====================")
    for barang in daftarBarang:
        st.write("Nama Barang:", barang["Nama Barang"])
        st.write("Harga: Rp{:,.0f}".format(barang["Harga"]))
        st.write("-----------------")
```
Fungsi ini digunakan untuk menampilkan daftar barang thrift ke antarmuka web menggunakan `streamlit`. Setiap barang ditampilkan dengan nama barang dan harganya.

4. Fungsi `cariBarang()`:
```
def cariBarang(daftarBarang, namaBarang):
    for barang in daftarBarang:
        if barang["Nama Barang"].lower() == namaBarang.lower():
            return barang
    return None
```
Fungsi ini digunakan untuk mencari barang dalam daftar berdasarkan nama barang yang diberikan. Fungsi ini akan mengembalikan dictionary barang jika ditemukan, dan None jika barang tidak ditemukan.

5. Fungsi `tambahUang()`:
```
def tambahUang(uang):
    input_uang = st.text_input("Masukkan jumlah uang yang ingin ditambahkan:")
    if input_uang.isnumeric() and int(input_uang) > 0:
        uang += int(input_uang)
        st.write("Uang berhasil ditambahkan.")
        st.write("Total uang Anda sekarang: Rp{:,.0f}".format(uang))
    else:
        st.write("Input tidak valid. Silakan masukkan jumlah uang yang valid.")
        return uang
```
Fungsi ini meminta pengguna untuk memasukkan jumlah uang yang ingin ditambahkan. Jika input valid, uang akan ditambahkan dan pesan sukses ditampilkan. Jika input tidak valid, pesan error ditampilkan dan nilai uang tidak berubah.

6. Fungsi `bayar()`:
```
def bayar(barang, uang):
    if uang >= barang["Harga"]:
        kembalian = uang - barang["Harga"]
        st.write("Pembayaran berhasil!")
        st.write("Kembalian Anda: Rp{:,.0f}".format(kembalian))
        uang = 0
    else:
        st.write("Maaf, uang Anda tidak cukup untuk membayar barang ini.")
        return uang
```
Fungsi ini digunakan untuk melakukan pembayaran. Jika uang yang dimiliki cukup, pesan pembayaran berhasil ditampilkan beserta jumlah kembalian. Nilai uang diatur menjadi 0. Jika uang tidak cukup, pesan error ditampilkan dan nilai uang tidak berubah.

7. Fungsi `panggilAPI()`:
```
def panggilAPI():
    response = requests.get("https://api.example.com/data")  # Ganti URL API dengan URL yang sesuai
    if response.status_code == 200:
        data = response.json()
        st.write("Data dari API:")
        st.write(data)
    else:
        st.write("Gagal memanggil API.")
```
Fungsi ini digunakan untuk memanggil API menggunakan library `requests`. Dalam contoh ini, fungsi ini melakukan permintaan GET ke URL API tertentu. Jika respon sukses (status kode 200), data dari respon JSON ditampilkan. Jika tidak, pesan error ditampilkan.

8. Tampilan Awal:

`tampilkanDaftarBarang(daftarBarang)`

Baris ini menampilkan daftar barang thrift saat program dijalankan.

9. Main Program:
```
uang = 0
menu = st.sidebar.selectbox("Menu:", ["Tambah Uang", "Cari Barang", "Panggil API", "Selesai"])
```
Variabel `uang` digunakan untuk melacak jumlah uang yang dimiliki oleh pengguna. Variabel `menu` adalah pilihan menu yang dipilih oleh pengguna melalui sidebar `streamlit`.

Selanjutnya, program memeriksa pilihan menu yang dipilih dan menjalankan fungsi yang sesuai:

- Jika menu "Tambah Uang" dipilih, fungsi `tambahUang()` akan dijalankan.
- Jika menu "Cari Barang" dipilih, pengguna diminta memasukkan nama barang yang ingin dicari melalui kotak teks. Jika nama barang valid, fungsi `cariBarang()` akan dijalankan untuk mencari barang tersebut.
- Jika menu "Panggil API" dipilih, fungsi `panggilAPI()` akan dijalankan untuk memanggil API.
- Jika menu "Selesai" dipilih, program akan menampilkan pesan penutup.

Setelah menjalankan fungsi yang sesuai, program akan terus berjalan dan menunggu interaksi pengguna melalui antarmuka web yang dihasilkan oleh `streamlit`.

- Penjelasan bagaimana sistem operasi digunakan dalam proses containerization

Dalam proses containerization, sistem operasi digunakan untuk:

1. Isolasi sumber daya antara container.
2. Pengaturan jaringan untuk container.
3. Manajemen sumber daya seperti CPU dan memori.
4. Pemutakhiran dan perbaikan sistem operasi.
5. Interaksi dengan container runtime seperti Docker atau Kubernetes.

Sistem operasi yang umum digunakan dalam containerization adalah Linux dengan kernel yang mendukung fitur-fitur container. Ada juga sistem operasi khusus untuk kontainer seperti CoreOS atau RancherOS.

- Penjelasan bagaimana containerization dapat membantu mempermudah pengembangan aplikasi

Containerization mempermudah pengembangan aplikasi dengan cara-cara berikut:

1. Lingkungan yang konsisten di seluruh siklus pengembangan.
2. Portabilitas aplikasi di berbagai lingkungan.
3. Skalabilitas yang mudah untuk menangani permintaan yang meningkat.
4. Isolasi dan keamanan aplikasi yang lebih baik.
5. Proses pengiriman yang cepat dan efisien.

Dengan menggunakan container, pengembang dapat menghindari masalah kompatibilitas, mempercepat proses pengiriman, meningkatkan skalabilitas, dan memastikan lingkungan yang konsisten.

- Penjelasan apa itu DevOps, bagaimana DevOps membantu pengembangan aplikasi

DevOps adalah pendekatan dalam pengembangan perangkat lunak yang menggabungkan praktik-praktik pengembangan (Development) dan operasional (Operations) secara terintegrasi. DevOps bertujuan untuk menciptakan alur kerja yang efisien, berkelanjutan, dan berkolaborasi antara tim pengembangan dan tim operasional.

DevOps membantu pengembangan aplikasi dengan beberapa cara:

1. Kolaborasi Tim yang Kuat: DevOps mendorong kolaborasi yang erat antara tim pengembangan dan tim operasional. Ini memungkinkan komunikasi yang lebih baik, pemahaman yang lebih mendalam tentang kebutuhan bisnis, dan koordinasi yang efisien dalam menghadapi tantangan dan perubahan dalam pengembangan aplikasi. Kolaborasi yang kuat memungkinkan pengembang untuk memahami persyaratan operasional dan memastikan bahwa aplikasi dapat diimplementasikan dan dikelola dengan lancar.

2. Automasi Proses dan Pengujian: DevOps menganjurkan otomatisasi dalam berbagai aspek pengembangan aplikasi. Automasi dapat diterapkan dalam tahap pembangunan, pengujian, pengiriman, dan pemeliharaan aplikasi. Dengan otomatisasi, tugas-tugas berulang dapat diatasi dengan cepat, memungkinkan pengembang untuk fokus pada inovasi dan pengembangan fitur baru. Automasi juga membantu memastikan kualitas aplikasi melalui pengujian otomatis yang terintegrasi.

3. Penerapan Continuous Integration/Continuous Deployment (CI/CD): DevOps mendorong penerapan CI/CD, yaitu pendekatan di mana perubahan kode secara terus-menerus diuji, diintegrasikan, dan diimplementasikan ke lingkungan produksi secara otomatis. CI/CD memungkinkan pengembang untuk dengan cepat menerapkan perubahan, mengurangi waktu penyebaran, dan mengurangi risiko kesalahan. Dengan adopsi CI/CD, pengembangan aplikasi menjadi lebih responsif dan dapat memenuhi kebutuhan pasar yang berubah dengan cepat.

4. Monitoring dan Tindak Lanjut: DevOps menekankan pentingnya pemantauan aplikasi secara terus-menerus dan respons cepat terhadap masalah yang muncul. Dengan menggunakan alat pemantauan dan logging yang tepat, tim DevOps dapat mendapatkan wawasan tentang kinerja aplikasi dan melakukan tindakan yang diperlukan untuk mengatasi masalah sebelum berdampak pada pengguna. Dengan demikian, DevOps membantu memastikan ketersediaan dan keandalan aplikasi secara terus-menerus.

5. Perubahan Budaya dan Mindset: DevOps melibatkan perubahan budaya dan mindset dalam pengembangan aplikasi. Ini mencakup kolaborasi tim lintas fungsi, penerapan tanggung jawab bersama, dan adopsi siklus umpan balik yang cepat untuk terus meningkatkan proses dan produk. DevOps mendorong budaya eksperimen, di mana pengembang dapat mencoba hal-hal baru dan memperbaiki proses secara berkelanjutan.

Secara keseluruhan, DevOps membantu pengembangan aplikasi dengan mengintegrasikan tim dan alur kerja, menerapkan otomasi, mengadopsi CI/CD, meningkatkan monitoring dan tindak lanjut, serta mengubah budaya dan mindset. Dengan pendekatan ini, pengembang dapat mengurangi waktu siklus pengembangan, meningkatkan kualitas, dan memberikan aplikasi yang lebih responsif dan andal kepada pengguna.

- Berikan contoh kasus penerapan DevOps di perusahaan / industri dunia nyata (contoh bagaimana implementasi DevOps di Gojek, dsb.)

Gojek, perusahaan teknologi terkemuka dalam industri layanan on-demand, telah menerapkan DevOps sebagai pendekatan yang fundamental dalam pengembangan aplikasi mereka. Berikut ini adalah contoh implementasi DevOps di Gojek:

1. Automasi Proses: Gojek menggunakan alat otomasi seperti Ansible, Terraform, dan Kubernetes untuk mengotomatiskan proses pengembangan dan pengiriman aplikasi. Dengan otomasi ini, Gojek dapat dengan cepat menyediakan infrastruktur, mengatur konfigurasi, dan melakukan pengujian otomatis. Proses pengembangan dan penyebaran menjadi lebih efisien dan konsisten, memungkinkan tim untuk lebih fokus pada inovasi dan pengembangan fitur.

2. Continuous Integration/Continuous Deployment (CI/CD): Gojek menerapkan CI/CD sebagai bagian dari alur kerja DevOps. Mereka menggunakan alat seperti Jenkins dan GitLab CI/CD untuk mengotomatiskan pengujian dan penyebaran aplikasi. Setiap kali ada perubahan kode yang diajukan, sistem CI/CD akan memicu serangkaian pengujian otomatis untuk memastikan kualitas kode. Jika pengujian berhasil, perubahan kode akan secara otomatis diterapkan ke lingkungan produksi. Pendekatan ini memungkinkan Gojek untuk menerapkan perubahan dengan cepat dan responsif, sehingga mengurangi waktu penyebaran dan meningkatkan keandalan aplikasi.

3. Kolaborasi Tim: Gojek mendorong kolaborasi yang erat antara tim pengembangan, tim operasional, dan tim keamanan. Mereka memastikan komunikasi yang terbuka dan saling mendukung antara tim-tim ini. Kolaborasi yang kuat memungkinkan pengembang untuk lebih memahami kebutuhan operasional, sementara tim operasional dapat memberikan masukan yang berharga dalam perancangan dan pengiriman aplikasi. Kebersamaan tim juga membantu mempercepat perbaikan masalah dan mengoptimalkan alur kerja.

4. Monitoring dan Pengelolaan Keandalan: Gojek sangat memperhatikan monitoring aplikasi dan pengelolaan keandalan layanan mereka. Mereka menggunakan berbagai alat seperti Grafana, Prometheus, dan ELK Stack untuk memantau kinerja aplikasi dan mendeteksi masalah dengan cepat. Tim operasional dapat merespons dengan cepat terhadap gangguan layanan, memperbaiki masalah, dan memastikan tingkat keandalan yang tinggi. Monitoring secara berkelanjutan juga membantu Gojek dalam meningkatkan kinerja aplikasi dan merespons perubahan yang diperlukan dengan cepat.

5. Adopsi Teknologi Cloud Native: Gojek telah mengadopsi teknologi cloud native seperti Kubernetes dan Docker untuk mengelola aplikasi mereka secara efisien. Dengan menggunakan teknologi ini, Gojek dapat mengelola skala aplikasi yang besar, memastikan elastisitas, dan dengan cepat melakukan penyebaran aplikasi ke berbagai lingkungan. Pendekatan cloud native juga memungkinkan Gojek untuk memanfaatkan fleksibilitas dan skalabilitas yang ditawarkan oleh platform cloud seperti AWS, GCP, dan Azure.

Melalui implementasi DevOps yang kokoh, Gojek telah berhasil meningkatkan efisiensi pengembangan aplikasi mereka, mempercepat waktu penyebaran, meningkatkan keandalan layanan, dan menghadapi persaingan bisnis yang ketat. Penerapan DevOps membantu Gojek untuk tetap responsif terhadap perubahan pasar dan memberikan pengalaman pengguna yang unggul dalam lingkungan teknologi yang terus berkembang.

# 6. Mampu menjelaskan dan mendemonstrasikan web page / web service yang telah dapat diakses public dalam bentuk video Youtube.

https://youtu.be/KVcGF7ztufI

# 7. Mampu mempublikasikan Docker Image yang telah dibuat ke Docker Hub, untuk memudahkan penciptaan container sistem operasi di berbagai server pada distributed computing.
![dokumentasi](dokumentasi/docker.jpg)

https://hub.docker.com/repository/docker/sidapaaaa/thrifting-pakaian/general
